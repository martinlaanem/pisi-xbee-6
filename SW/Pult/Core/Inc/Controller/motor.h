
#ifndef INC_CONTROLLER_MOTOR_H_
#define INC_CONTROLLER_MOTOR_H_


#include "main.h"


extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim9;

void Motors_Init();
void Motor_Set(int32_t left, int32_t right);


#endif /* INC_CONTROLLER_MOTOR_H_ */
