/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "sensors.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define LEFT 	0
#define RIGHT	1
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
int8_t left, right = 0;
uint8_t motors[2] = {0, 0};
uint8_t pData = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void map_joystick(uint16_t x, uint16_t y);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_TIM3_Init();
  MX_TIM9_Init();
  MX_USART3_UART_Init();
  MX_ADC_Init();
  /* USER CODE BEGIN 2 */
  HAL_UART_Receive_IT(&huart3, &pData, 1);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  uint16_t x_pos, y_pos;


  while (1)
  {
	  x_pos = ADC_Read(&hadc, 3);
	  y_pos = ADC_Read(&hadc, 4);

	  map_joystick(x_pos, y_pos);

	  motors[LEFT] = left;
	  motors[RIGHT] = right;

	  HAL_UART_Transmit(&huart3, motors, 2, 100);
	  HAL_Delay(10);


    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart)
{
	/*if (pData != 0)
	{
		HAL_UART_Transmit(&huart3, &pData, 1, 1000);
	}*/
	//HAL_GPIO_TogglePin(GPIOB, OUT_RGB_1_Pin);
	HAL_UART_Receive_IT(&huart3, &pData, 1);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin == BTN1_Pin)
	{
		motors[0] = 70;
		motors[1] = 70;
	}
	if (GPIO_Pin == BTN2_Pin)
	{
		motors[0] = 0;
		motors[1] = 0;
	}
	HAL_UART_Transmit(&huart3, motors, 2, 100);
}

void map_joystick(uint16_t x, uint16_t y)
{
/*
	if (x > 2050 && x < 2150)
		x = 2000;
	if (y > 1950 && y < 2050)
		y = 2000;
	float x_nullpunktist = ((float)(x) - 2000.0) / 2000.0;
	float y_nullpunktist = ((float)(y) - 2000.0) / 2000.0;

	float left_f = (y_nullpunktist + x_nullpunktist) * 90.0;
	float right_f = (y_nullpunktist - x_nullpunktist) * 90.0;

	left = (uint8_t)left_f;
	right = (uint8_t)right_f;*/

	if (y < 1700)
	{
		if (x < 1700)
		{
			left = 70;
			right = 50;
		}
		else if (x > 1700 && x < 2300)
		{
			left = 70;
			right = 70;
		}
		else
		{
			left = 50;
			right = 70;
		}
	}
	else if (y > 1700 && y < 2300)
	{
		if (x < 1700)
		{
			left = 50;
			right = 0;
		}
		else if (x > 1700 && x < 2300)
		{
			left = 0;
			right = 0;
		}
		else
		{
			left = 0;
			right = 50;
		}
	}
	else
	{
		if (x < 1700)
		{
			left = -70;
			right = -50;
		}
		else if (x > 1700 && x < 2300)
		{
			left = -70;
			right = -70;
		}
		else
		{
			left = -50;
			right = -70;
		}
	}

	if (left < 0)
	{
		left = (0 - left) + 100;
	}
	if (right < 0)
	{
		right = (0 - right) + 100;
	}


}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
