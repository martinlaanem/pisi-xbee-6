
#include "sensors.h"

/*
 * @brief  Sends a light pulse from GPIO and reads the value from ADC
 *
 * @param  hadc, pointer to ADCx handle
 * @param  sensorID, specifies which ADCx channel sensor should use (1-4)
 * @param  GPIOx where x can be (A..F) to select the GPIO peripheral for STM32F3 family
 * @param  GPIO_Pin specifies the port bit to be written.
 *         This parameter can be one of GPIO_PIN_x where x can be (0..15).
 *
 * @retval value, read from ADC_Read()
 */

uint16_t ADC_Read(ADC_HandleTypeDef* hadc, uint8_t channel)
{
	ADC_ChannelConfTypeDef sConfig;
	uint8_t set_channel;

	switch (channel)
	{
		case 1:
			set_channel = ADC_CHANNEL_1;
		case 2:
			set_channel = ADC_CHANNEL_2;
			break;
		case 3:
			set_channel = ADC_CHANNEL_3;
			break;
		case 4:
			set_channel = ADC_CHANNEL_4;
			break;
		case 5:
			set_channel = ADC_CHANNEL_5;
			break;
		case 6:
			set_channel = ADC_CHANNEL_6;
			break;
		default:
			return 0;
	}

	sConfig.Channel = set_channel;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLETIME_9CYCLES;

	if (HAL_ADC_ConfigChannel(hadc, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}

	HAL_ADC_Start(hadc);
	HAL_ADC_PollForConversion(hadc, 500);

	return HAL_ADC_GetValue(hadc);
}

#ifdef LAB_LED_EN_GPIO_Port
uint16_t Read_Dist_IR(ADC_HandleTypeDef* hadc, uint8_t channel)
{
	uint16_t value;

	HAL_GPIO_WritePin(LAB_LED_EN_GPIO_Port, LAB_LED_EN_Pin, GPIO_PIN_SET);
	value = ADC_Read(hadc, channel);
	HAL_GPIO_WritePin(LAB_LED_EN_GPIO_Port, LAB_LED_EN_Pin, GPIO_PIN_RESET);

	return value;
}

uint16_t Read_Line(ADC_HandleTypeDef* hadc, uint8_t channel)
{
	uint16_t value;

	HAL_GPIO_WritePin(LAB_LED_EN_GPIO_Port, LAB_LED_EN_Pin, GPIO_PIN_SET);
	value = ADC_Read(hadc, channel);
	HAL_GPIO_WritePin(LAB_LED_EN_GPIO_Port, LAB_LED_EN_Pin, GPIO_PIN_RESET);

	return value;
}
#endif


