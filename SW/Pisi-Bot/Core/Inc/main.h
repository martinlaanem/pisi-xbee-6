/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define DRV_MODE_EN_Pin GPIO_PIN_15
#define DRV_MODE_EN_GPIO_Port GPIOC
#define OUT_RGB_1_Pin GPIO_PIN_0
#define OUT_RGB_1_GPIO_Port GPIOB
#define OUT_RGB_2_Pin GPIO_PIN_1
#define OUT_RGB_2_GPIO_Port GPIOB
#define OUT_RGB_3_Pin GPIO_PIN_2
#define OUT_RGB_3_GPIO_Port GPIOB
#define BUZZER_PWM_Pin GPIO_PIN_12
#define BUZZER_PWM_GPIO_Port GPIOB
#define MOTOR_A1_Pin GPIO_PIN_13
#define MOTOR_A1_GPIO_Port GPIOB
#define MOTOR_A2_Pin GPIO_PIN_14
#define MOTOR_A2_GPIO_Port GPIOB
#define IN_BUTTON2_Pin GPIO_PIN_15
#define IN_BUTTON2_GPIO_Port GPIOB
#define IN_BUTTON1_Pin GPIO_PIN_9
#define IN_BUTTON1_GPIO_Port GPIOA
#define LAB_LED_EN_Pin GPIO_PIN_10
#define LAB_LED_EN_GPIO_Port GPIOA
#define ENCODER_2_2_Pin GPIO_PIN_15
#define ENCODER_2_2_GPIO_Port GPIOA
#define ENCODER_2_1_Pin GPIO_PIN_3
#define ENCODER_2_1_GPIO_Port GPIOB
#define MOTOR_B1_Pin GPIO_PIN_4
#define MOTOR_B1_GPIO_Port GPIOB
#define MOTOR_B2_Pin GPIO_PIN_5
#define MOTOR_B2_GPIO_Port GPIOB
#define ENCODER_1_2_Pin GPIO_PIN_6
#define ENCODER_1_2_GPIO_Port GPIOB
#define ENCODER_1_1_Pin GPIO_PIN_7
#define ENCODER_1_1_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
