
#ifndef INC_CONTROLLER_COM_H_
#define INC_CONTROLLER_COM_H_

#include "main.h"

extern UART_HandleTypeDef huart3;


void Radio_Receive(uint8_t *pData, uint16_t Size, uint32_t Timeout);
void Radio_Transmit(uint8_t *pData, uint16_t Size);
void Radio_Putc(uint8_t* character);
void Radio_Getc(uint8_t* character, uint32_t Timeout);

#endif /* INC_CONTROLLER_COM_H_ */
