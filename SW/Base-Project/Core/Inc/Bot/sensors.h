
#ifndef INC_BOT_SENSORS_H_
#define INC_BOT_SENSORS_H_

#include "adc.h"

uint16_t ADC_Read(ADC_HandleTypeDef* hadc, uint32_t channel);
uint16_t Read_Dist_IR(ADC_HandleTypeDef* hadc, uint8_t channel);
uint16_t Read_Line(ADC_HandleTypeDef* hadc, uint8_t channel);

#endif /* INC_BOT_SENSORS_H_ */
