
#ifndef INC_CONTROLLER_BOARD_H_
#define INC_CONTROLLER_BOARD_H_

#include "main.h"

#define RED			((uint8_t)0b00000100)
#define GREEN		((uint8_t)0b00000010)
#define BLUE		((uint8_t)0b00000001)
#define YELLOW		((uint8_t)0b00000110)
#define PINK		((uint8_t)0b00000101)
#define CYAN		((uint8_t)0b00000011)

uint8_t button1_read(void);
uint8_t button2_read(void);
void RGB_Set(uint8_t color);
void buzzer_init(void);
void buzzer(uint16_t value);

int16_t position;
extern TIM_HandleTypeDef htim10;

#endif /* INC_CONTROLLER_BOARD_H_ */
