/*
 * gyro.h
 *
 *  Created on: Oct 16, 2019
 *      Author: erki
 */

#ifndef PERIPHERALS_GYRO_H_
#define PERIPHERALS_GYRO_H_

#include "i2c.h"

typedef enum
{
	GYRO_AXIS_X = 0,
	GYRO_AXIS_Y = 1,
	GYRO_AXIS_Z = 2
} PER_Gyro_Axis_e;

typedef enum
{
	GFS_250DPS = 0,
	GFS_500DPS = 1,
	GFS_1000DPS = 2,
	GFS_2000DPS = 3
} GyroFullScaleRange;

typedef enum
{
	AFS_2G = 0,
	AFS_4G = 1,
	AFS_8G = 2,
	AFS_16G = 3
} AccelFullScaleRange;

typedef struct
{
  I2C_HandleTypeDef* hi2c;
} PER_Gyro_t;

extern float GyroScale[4];
extern float AccelScale[4];

PER_Gyro_t PER_Gyro_Init(I2C_HandleTypeDef* i2c);
void PER_Gyro_CalculateBias(float* gyro_bias, float* accel_bias, PER_Gyro_t* icm);
void PER_Gyro_CalibrateIcm(PER_Gyro_t* icm);
void PER_Gyro_ReadGyro(float* gyro, PER_Gyro_t* icm);
void PER_Gyro_ReadGyroRaw(int16_t* gyro, PER_Gyro_t* icm);
void PER_Gyro_ReadAccel(float* accel, PER_Gyro_t* icm);
void PER_Gyro_ReadAccelRaw(int16_t* accel, PER_Gyro_t* icm);
float PER_Gyro_ProcessGyroAxis(uint8_t* data, PER_Gyro_Axis_e axis);
float PER_Gyro_ProcessAccelAxis(uint8_t* data, PER_Gyro_Axis_e axis);
void PER_Gyro_SetAccelFullScaleRange(AccelFullScaleRange fsr, PER_Gyro_t* icm);
void PER_Gyro_SetGyroFullScaleRange(GyroFullScaleRange fsr, PER_Gyro_t* icm);
void PER_Gyro_SetLocalGyroBias(float* bias);
void PER_Gyro_SetLocalAccelBias(float* bias);

#endif /* PERIPHERALS_GYRO_H_ */
