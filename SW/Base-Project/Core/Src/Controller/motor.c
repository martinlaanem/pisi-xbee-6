#include "motor.h"

//#define DRV_MODE_EN		1
#define PWM_MAX			255


void Motors_Init()
{
	//Pull driver mode low, since the other mode isn't implemented correctly
	HAL_GPIO_WritePin(DRV_MODE_EN_GPIO_Port, DRV_MODE_EN_Pin, GPIO_PIN_RESET);

	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim9, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim9, TIM_CHANNEL_2);
}

/*
 * @Description: 	The left and right values are the duty cycle/speed percentage.
 * 					100 is max speed and shouldn't be exceeded.
 * @Notes: 			Motors need at least 50%+ duty cycle to get going from standstill.
 * 					Once motors are moving the min speed duty cycle is about 30%.
 * 					Below that, motors stop working.
 *
 * Left motor 	-> TIM3
 * Right motor 	-> TIM9
 * xIN1 		-> channel 1
 * xIN2 		-> channel 2
 */
void Motor_Set(int32_t left, int32_t right)
{
#ifdef DRV_MODE_EN
	if (left < 0)
	{
		TIM3->CCR1 = (uint32_t)(-left);
		TIM3->CCR2 = PWM_MAX;
	}
	else
	{
		TIM3->CCR1 = left;
		TIM3->CCR2 = 0;
	}
	if (right < 0)
	{
		TIM9->CCR1 = (uint32_t)(-right);
		TIM9->CCR2 = PWM_MAX;
	}
	else
	{
		TIM9->CCR1 = right;
		TIM9->CCR2 = 0;
	}

#else
	if (left < 0)
	{
		  TIM3->CCR1 = 0;
		  TIM3->CCR2 = (uint32_t)(-left);
	}
	else
	{
		  //TIM3->CCR1 = 0;
		  //TIM3->CCR2 = (uint32_t)(left);
		  TIM3->CCR1 = (uint32_t)(left);
		  TIM3->CCR2 = 0;
	}
	if (right < 0)
	{
		  TIM9->CCR1 = (uint32_t)(-right);
		  TIM9->CCR2 = 0;
	}
	else
	{
		  TIM9->CCR1 = 0;
		  TIM9->CCR2 = (uint32_t)(right);
	}
#endif
}








