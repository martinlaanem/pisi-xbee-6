
#include "board.h"
#include "tim.h"

uint8_t button1_read()
{
	uint8_t status = HAL_GPIO_ReadPin(GPIOB, IN_BUTTON1_Pin);
	return status;
}

uint8_t button2_read()
{
	uint8_t status = HAL_GPIO_ReadPin(GPIOB, IN_BUTTON2_Pin);
	return status;
}

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
	//Left wheel
	if (htim == &htim4)
	{
		position = (__HAL_TIM_GET_COUNTER(htim)) / 1; //Divide by click incremental value

		//Determine how many clicks per revolution to calculate distance driven
	}

	//Right wheel
	else if (htim == &htim4)
	{
		position = (__HAL_TIM_GET_COUNTER(htim)) / 1; //Divide by click incremental value

		//Determine how many clicks per revolution to calculate distance driven
	}

}

void RGB_Set(uint8_t color)
{
	uint8_t state;

	//Set red led
	state = (color & RED) ? 1 : 0;
	HAL_GPIO_WritePin(GPIOB, OUT_RGB_1_Pin, state);

	//Set green led
	state = (color & GREEN) ? 1 : 0;
	HAL_GPIO_WritePin(GPIOB, OUT_RGB_2_Pin, state);

	//Set blue led
	state = (color & BLUE) ? 1 : 0;
	HAL_GPIO_WritePin(GPIOB, OUT_RGB_3_Pin, state);
}

//htim4
void buzzer_init(void)
{
	HAL_TIM_Base_Start(&htim10);
	HAL_TIM_PWM_Start(&htim10, TIM_CHANNEL_1);
}

//Tim channel htim4
//value range - ?
void buzzer(uint16_t value)
{
	if (value)
	{
		__HAL_TIM_SET_COMPARE(&htim10, TIM_CHANNEL_1, value);
	}
}
