#include "com.h"



void Radio_Receive(uint8_t* pData, uint16_t Size, uint32_t Timeout)
{
	HAL_UART_Receive(&huart3, pData, Size, Timeout);
}

void Radio_Transmit(uint8_t* pData, uint16_t Size)
{
	HAL_UART_Transmit(&huart3, pData, Size, 0);
}

void Radio_Putc(uint8_t* character)
{
	HAL_UART_Transmit(&huart3, character, 1, 0);
}

void Radio_Getc(uint8_t* character, uint32_t Timeout)
{
	HAL_UART_Receive(&huart3, character, 1, Timeout);
}


