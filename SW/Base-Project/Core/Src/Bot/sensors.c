
#include "sensors.h"

/*
 * @brief  Sends a light pulse from GPIO and reads the value from ADC
 *
 * @param  hadc, pointer to ADCx handle
 * @param  sensorID, specifies which ADCx channel sensor should use (1-4)
 * @param  GPIOx where x can be (A..F) to select the GPIO peripheral for STM32F3 family
 * @param  GPIO_Pin specifies the port bit to be written.
 *         This parameter can be one of GPIO_PIN_x where x can be (0..15).
 *
 * @retval value, read from ADC_Read()
 */

uint16_t ADC_Read(ADC_HandleTypeDef* hadc, uint32_t channel)
{
	ADC_ChannelConfTypeDef sConfig;

	sConfig.Channel = channel;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLETIME_9CYCLES;

	if (HAL_ADC_ConfigChannel(hadc, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}

	HAL_ADC_Start(hadc);
	HAL_ADC_PollForConversion(hadc, 500);

	return HAL_ADC_GetValue(hadc);
}

#ifdef LAB_LED_EN_GPIO_Port
uint16_t Read_Dist_IR(ADC_HandleTypeDef* hadc, uint8_t channel)
{
	uint16_t value;

	HAL_GPIO_WritePin(LAB_LED_EN_GPIO_Port, LAB_LED_EN_Pin, GPIO_PIN_SET);
	value = ADC_Read(hadc, channel);
	HAL_GPIO_WritePin(LAB_LED_EN_GPIO_Port, LAB_LED_EN_Pin, GPIO_PIN_RESET);

	return value;
}

/*IR EN jumper needs to be connected on Pisi-Bot to turn on line sensors transmitter */
uint16_t Read_Line(ADC_HandleTypeDef* hadc, uint8_t channel)
{
	uint16_t value;

	value = ADC_Read(hadc, channel);

	return value;
}
#endif


